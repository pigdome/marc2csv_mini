use strict;
use MARC::Batch;
use MARC::Record;
use MARC::File::USMARC;
use utf8;
use Text::CSV;


my $map_header = $ARGV[2];


open( OUTPUT_CSV, '> ' . $ARGV[1] ) or die $!;
open( OUTPUT_LOG , '> log.txt' ) or die $!;

my $batch = MARC::Batch->new('USMARC',$ARGV[0]);
$batch->strict_off();
$batch->warnings_off();


my $DEBUG = 0;
my $current_record = "";
my %row = ();

binmode(OUTPUT_CSV, ':utf8');
binmode(OUTPUT_LOG, ':utf8');
binmode(STDOUT, ':utf8');

#--------------------------------------------
my $csv = Text::CSV->new ( { binary => 0 } ) or die "Cannot use CSV: ".Text::CSV->error_diag ();
open(my $fh_map_header, '<:encoding(UTF-8)', $map_header) or die "Could not open file '$map_header' $!";

my %hash_map_header = ();
my @header_row = ();
while ( my $row = $csv->getline( $fh_map_header ) )
{
	$row->[0] =~ s/^\s+|\s+$//g;
	$row->[1] =~ s/^\s+|\s+$//g;
    $row->[2] =~ s/^\s+|\s+$//g;

    $hash_map_header{$row->[0]}{$row->[1]} = $row->[2];
    push @header_row , $row->[2];
}
close $fh_map_header;
#--------------------------------------------
@header_row = sort @header_row;
foreach my $header (@header_row) 
{
	print OUTPUT_CSV "\"$header\",";
}
print OUTPUT_CSV "\n";

convertMARCToCSV();

close( OUTPUT_CSV );
close( OUTPUT_LOG );

sub convertMARCToCSV
{
	my $record;
	eval { $record = $batch->next(); };
    while ( $record )
    {
    	#$record->encoding('UNICODE');
    	$current_record = $record->field("001")->as_string();
    	foreach my $field ($record->field("...")) 
	    {
	       my $log;
	       my @Field = ();
	       if( $field->tag() < 10 )
	       {
	       	   getFieldLower10( $field , $record->field( $field->tag() ) ) ;
	       }
	       else
	       {
    		   getFieldUpper10( $field );
	       }
	    }

	    my %hash_temp = ();
        
        # group by tag
	    foreach my $key ( keys %row )
	    {
	    	my $data = $row{$key};
	    	if( $hash_temp{$key} )
	    	{ 
	    		$hash_temp{$key} = $hash_temp{$key} . "||" . $data;
	    	}
	    	else
	    	{
	    		$hash_temp{$key} = $data;
	    	}
	    }
        %row = %hash_temp;
	    # print 
        foreach my $key ( @header_row ) 
		{
			print OUTPUT_CSV "\"$row{$key}\",";
		}
        print OUTPUT_CSV "\n";
	    # clear for next record
        %row = ();
	    
	    READ_BATCH:
	    if ( my @warnings = $batch->warnings() ) 
	    {
           plog( "WARNINGS :" . @warnings . "\n" );
        }
	    do 
	    {
           eval { $record = $batch->next(); };
           if ($@){ plog( "ERROR next to bib#  (skipped):\n" . $@ . "\n"); }
        } 
        while $@;

    }
        
}


sub getFieldUpper10
{
	my ( $field ) = @_;
	my $tag = $field->tag();
	my @subfields = $field->subfields();
    my %hash = ();
    my $count = 0;
	while ( my $subfield = shift( @subfields ) ) 
	{

		my ( $code , $data ) = @$subfield;
        my $header = $hash_map_header{$tag}{$code};
        
        if($header) 
        {
        	addToHash( $header , $data );
        }
	}
}




sub getFieldLower10
{
	my ( $field,$field_u10 ) = @_;
    my @Field = ();
    my $tag = $field->tag();
	my $data = $field_u10->as_string();
	my $header = $hash_map_header{$tag}{''};

	if($header) 
    {
    	addToHash( $header , $data );
    }
}

sub addToHash
{
	my ($key,$value) = @_;
	if( $row{$key} )
	{
		$row{$key} = $row{$key} . "||" . $value;
	}
	else
	{
		$row{$key} = $value;
	}
}

sub plog 
{
	my ($log) = @_;
	foreach my $x (@ARGV) 
	{
	  	if ($x eq '-d' || $x eq '-D') 
	  	{
	  	 	$DEBUG = 1;
	  	}
	}
	print OUTPUT_LOG $log if $DEBUG; 	
}









































 
    
